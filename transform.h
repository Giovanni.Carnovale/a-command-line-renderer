#pragma once
#include "quaternion.h"

namespace mgd {

class Transform{
public:
  Scalar scale;
  Vector3 translate;
  Quaternion rotate;

  Transform(): scale(1), translate(0,0,0), rotate(Quaternion::identity()){
  }

  Vector3 transformPoint(const Vector3 &p) const{
      return rotate.apply(p * scale) + translate;
  }
  Vector3 transformVersor(const Vector3 &p) const{
      return rotate.apply(p);
  }
  Vector3 transformVector(const Vector3 &p) const{
      return rotate.apply(p * scale);
  }
  Scalar transformScalar(Scalar p) const{
      return p * scale;
  }

  Transform inverse() const {
      Transform t;
      t.scale = (1/scale);
      t.rotate = rotate.conjugated();
      t.translate = t.rotate.apply(-translate * t.scale);
      // oppure: t.translarte = t.applyToVector( -translate );
      return t;
  }

  void invert(){
      scale = (1/scale);
      rotate.conjugate();
      translate = rotate.apply(-translate * scale);
      // oppure: translate = applyToVector(-translate);
  }
};


bool operator==(const Transform& a, const Transform& b) {
	return a.translate == b.translate && a.scale == b.scale && a.rotate == b.rotate;
}

//  first b then a
Transform operator * (const Transform &a, const Transform &b) {
   Transform t;
   t.rotate = a.rotate * b.rotate;
   t.scale = a.scale * b.scale;
   t.translate = a.transformVector(b.translate) + a.translate;
   return t;
}

Transform Lerp(const Transform& a, const Transform& b, Scalar t) {
    Transform result = Transform();
    
    result.scale = (1-t)*a.scale + t*b.scale;
    result.rotate = Slerp(a.rotate, b.rotate, t);
    result.translate = (1-t)*a.translate + t*b.translate;

    return result;
}

}
