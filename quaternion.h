#pragma once
#define _USE_MATH_DEFINES
#include <math.h>
#include "vector3.h"


namespace mgd {

class Quaternion{
public:
    Vector3 im;
    Scalar re;

    Quaternion(Vector3 _im, Scalar _re):im(_im),re(_re){ }
    Vector3 apply(Vector3 p) const;
    void conjugate(){
        im = -im;
    }
    Quaternion conjugated() const {
        return Quaternion(-im,re);
    }

	inline Quaternion operator * (Scalar k) const { return Quaternion(k*im, k*re); }
	inline Quaternion operator / (Scalar k) const { return Quaternion(im/k, re/k); }

    Quaternion operator -() const {
        return Quaternion(-im,-re);
    }

    static Quaternion fromVector3(Vector3 v){
        return Quaternion(v,0);
    }
    static Quaternion fromAngleAxis(Scalar deg, Vector3 axis){
        Scalar rad = deg * M_PI / 180;
        Scalar s = std::sin(rad/2);
        Scalar c = std::cos(rad/2);
        return Quaternion(axis.normalized() * s, c);
    }

    static Quaternion identity() {
        return Quaternion( Vector3(0,0,0) , 1 );
    }

    inline float norm() const {
        return std::sqrt(im.squaredNorm() + re*re);
    }

    inline void normalize() {
        (*this) = (*this) * norm();
    }

	Quaternion normalized() const {
		return (*this) / norm();
	}

};

inline Scalar dot(const Quaternion& a, const Quaternion& b) {
	return dot(a.im, b.im) + a.re * b.re;
}

inline Quaternion operator * (float a, const Quaternion& b) {
	return b*a;
}

inline Quaternion operator * (const Quaternion& a, const Quaternion& b) {
	return Quaternion(
		cross(a.im, b.im) + a.im * b.re + a.re * b.im,
		-dot(a.im, b.im) + a.re * b.re
	);
}

inline Quaternion operator+ (const Quaternion& a, const Quaternion& b) {
	return Quaternion(a.im + b.im, a.re + b.re);
}

inline bool operator== (const Quaternion& a, const Quaternion& b) {
	return a.im == b.im && a.re == b.re;
}

inline bool areEqual(const Quaternion& a, const Quaternion& b) {
	return areEqual(a.im, b.im) && areEqual(a.re, b.re);
}

inline bool areEquivalent(const Quaternion& a, const Quaternion& b) {
	return areEqual(a, b) || areEqual(a, -b);
}

inline Quaternion Slerp(const Quaternion& a, const Quaternion& b, Scalar t) {

    Scalar distance1 = 1 - dot(a, b) * dot(a, b);
    Scalar distance2 = 1 - dot(a, -b) * dot(a, -b);

    if (distance1 <= distance2) {
        return ((1-t)*a + t * b).normalized();
    }
    else {
        return ((1 - t) * a + t * (-b)).normalized();
    }
}

inline Vector3 Quaternion::apply(Vector3 p) const {
    Quaternion q = fromVector3(p);
    q = (*this) * q * this->conjugated();
    assert(areEqual( q.re, 0 ));
    return q.im;
}

std::ostream& operator<<(std::ostream& os, const Quaternion& quat) {
	os << "(" << quat.im.x << ", "<<quat.im.y << ", "<<quat.im.z << ", " << quat.re << ")";
	return os;
}

}
