# A Command Line Renderer
This project shows a simple 3D graphic library to render shapes into the command line. It was developed as the final project in the Advanced Graphic Programming Course of a Game Development Course.

The main focus was implementing various mathematics-related function: such as **Quaternions**, **Vectors**, **Transformations** and so on.

## Description
___

The Mathematical side of the project is focused on implementing **3D Vectors**, **Quaternion**, and **Transformations**.

Each of these has been implemented has a class with its operators overloaded to achieve a simple interface.
These classes also have methods for *NLerping*, applying transformations to other objects such as *points*, *vectors* and *versors* and so on.

On the more technical side, the project is composed of a **Mesh** interface, composed of a *raycast function* and an *apply function*. The project comes with two implementations of the interface: **Sphere** and **Plane**.

The **GameObj** class is defined, containing an array of **Mesh** objects pointers and a **Transform**, in this way it is possible to reutilize the same mesh in multiple objects.

The rendering is executed by the two classes **Camera**, encapsulating the position and some Camera parameters, and the **Scene** class.

The **Scene** will return vectors of meshes transformed either in *World Space* or in *View Space* given a camera Transform, which will be used by the raycast function

``` cpp
void rayCasting(const std::vector<Mesh*>& meshesVector){

    Camera c(5, 64,52);
    HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD tl = {0,0};
    SetConsoleCursorPosition(console, tl);

    std::string screenBuffer ; // a string to get ready and print all at once

    for (int y = 0; y<c.pixelDimY; y++) {
        for (int x= 0; x<c.pixelDimX; x++) {
            Point3 hitPos;
            Point3 hitNorm;
            Scalar distMax = 100.0;

            for  (Mesh* s : meshesVector) {
               s->rayCast( c.primaryRay(x,y)  , hitPos , hitNorm , distMax );
            }

            screenBuffer += lighting( hitNorm );
        }
        screenBuffer +=  "\n";
    }
    std::cout << screenBuffer;
}
```

## The Demo
___
The project comes with a demo, which populates the scene with some spheres and a plane to act as a floor. It uses some *WinAPI* function to get keyboard inputs and allow the user to move the camera.

### Controls:
* **WASD**: to move or rotate;
* **1~9**: change current moved object to one of the gameobj in the scene; 
* **0**: return to world origin, leaving any previously controlled gameobj;
* **Shift + 0~9**: move to choosen gameobj WITHOUT changing the tracked object, when you leave shift you will go back to the tracked object position;
* **Spacebar**: move the camera on an higher position to observe the scene from above;

The Demo also uses the lerping functions to move smoothly the camera.

## Installation
___
There is no special installation needed. You can open the Visual Studio file and build the game yourself.
