#pragma once

#include <vector>
#include "vector3.h"
#include "transform.h"

namespace mgd {


struct Ray{
    Point3 p;
    Versor3 d;

    Ray( Point3 _p, Vector3 _d) : p(_p), d( _d.normalized() ) { }
    Ray(){}
};

struct Mesh {
    virtual bool rayCast(Ray ray, Point3& hitPos, Versor3& hitNorm, float& distMax) const = 0;
	virtual Mesh* apply(const Transform& t) const = 0;
};

struct Sphere : public Mesh{ 
    Point3 c;
    Scalar r;
    Sphere(Point3 _c, Scalar _r):c(_c),r(_r){}

	virtual bool rayCast(Ray ray, Point3& hitPos, Versor3& hitNorm, float& distMax) const override{
		// see exercise on paper...
		// the hitpos is (ray.p + k * ray.dir)
		// for some k such that a*k^2 + b*k + c  = 0
		Scalar a = 1;
		Scalar b = 2 * dot(ray.d, ray.p - this->c);
		Scalar c = (ray.p - this->c).squaredNorm() - this->r * this->r;

		Scalar delta = b * b - 4 * a * c;

		if (delta < 0) return false; // ray misses the sphere!

		Scalar k = (-b - sqrt(delta)) / (2 * a);
		if (k < 0) return false;
		if (k > distMax) return false;
		distMax = k;

		hitPos = ray.p + k * ray.d;
		hitNorm = (hitPos - this->c).normalized();
		return true;

	}


	Sphere* apply(const Transform& a) const override {
		return new Sphere(
			a.transformPoint(c),
			a.transformScalar(r)
		);
	}

};

struct Plane : public Mesh { // POS
    Point3 p;
    Versor3 n;
    Plane( Point3 _p, Versor3 _n):p(_p),n(_n){}


	virtual bool rayCast(Ray ray, Point3& hitPos, Versor3& hitNorm, float& distMax) const override {
		Scalar dn = dot(ray.d, n);
		if (dn == 0) return false;

		Scalar k = dot(p - ray.p, n) / dn;

		if (k < 0) return false;
		if (k > distMax) return false;
		distMax = k;
		hitPos = ray.p + k * ray.d;
		hitNorm = n;
		return true;
	}

	Plane* apply(const Transform& a) const override {
		return new Plane(
			a.transformPoint(p),
			a.transformVersor(n)
		);
	}
};



class GameObj {
public:
	Transform transform;

	std::vector<Mesh*> meshArray;

	GameObj() :
		transform(),
		meshArray()
	{
	}

	GameObj(std::initializer_list<Mesh*> mesh) : transform(), meshArray(mesh) {}
};
} // end of namespace


