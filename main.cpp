#include <iostream>
#include <chrono>
#include <windows.h>

#include "vector3.h"
#include "shapes3d.h"
#include "camera.h"

#include "quaternion.h"
#include "transform.h"

#include "scene.h"


using namespace mgd;

void unitTestLinearOps(){

    Vector3 a(24,1,32), b(12,3,54);
    Scalar k = 5.0;
    assert( areEqual( a+b , b+a ) );
    assert( areEqual( (a+b)*k , b*k + a*k ) );
}

void unitTestProdutcs(){
    Vector3 a(24,-3,32), b(12,3,-54), c(10,13,-43);
    Scalar k = 0.4f;

    assert( isZero( dot( a , cross(a,b) ) ) );
    assert( isZero( dot( b , cross(a,b) ) ) );

    assert( isZero( cross(a,b) + cross(b,a) ) );

    assert( areEqual( dot(k*a,b) , dot(b,k*a) ) );
    assert( areEqual( cross(a,b+4*c) , cross(a,b)+4*cross(a,c) ) );
    assert( areEqual( cross(2*a,2*b) , 4*cross(a,b) ) );
}

void unitTestRaycasts(){
    Ray r( Point3(0,0,0), Vector3(1,0,0) );
    Sphere s( Point3(5,0,0) , 3 );
    Point3 hitPoint;
    Point3 hitNorm;
    Scalar distMax = 100000;

    bool isHit = s.rayCast(r,hitPoint,hitNorm,distMax);
    assert(isHit);
    assert(areEqual( hitPoint, Point3(2,0,0) ));
}

void unitTestRaycastPlane(){
    Ray r( Point3(0,0,0), Vector3(1,0,0) );
    Plane p( Point3(10,0,0), Vector3(-1,0,0) );
    Point3 hitPoint;
    Point3 hitNorm;
    Scalar distMax = 100000;

    bool isHit = p.rayCast(r,hitPoint,hitNorm,distMax);
    assert(isHit);
    assert(areEqual( hitPoint, Point3(10,0,0) ));
}


float currentTime(){
    static float now = 0.0;
    now += 0.005;
    return now;
    //return std::chrono::system_clock().now();
}




void examplesOfSyntax(){
    Vector3 v(0,2,3);

    Vector3 r = v * 4;  //Vector3 r = v.operator *(4);
    r *= 0.25;
    Vector3 w = v + r; //  Vector3 w = v.operator + (r);
    v += w;

    v = r-v;
    v = -w + v;
    Scalar k = dot(v,w);

    v.x = 0.4f;

    Scalar h = v[0];

    // v[6] = 0.3;
}

void unitTestQuaternions(){

    {
    Quaternion rot = Quaternion::fromAngleAxis(180,Vector3(0,1,0));
    Vector3 p(0,0,1);
    Vector3 q = rot.apply(p);
    assert(areEqual(q, Vector3(0,0,-1)));
    }

    {
    Quaternion rot = Quaternion::fromAngleAxis(90,Vector3(0,1,0));
    Vector3 p(3,5,6);
    Vector3 q = rot.apply(p);
    assert(areEqual(q, Vector3(6,5,-3)));
    }


    Quaternion rot = Quaternion::fromAngleAxis(30,Vector3(1,1,3));
    Vector3 p(3,5,6), q = p;
    for (int k=0; k<12; k++) q = rot.apply(q);
    assert(areEqual(q,p));



    {
        Quaternion q = Quaternion::identity();

        Vector3 randomAxis(4,3,1);
        Scalar deg = 90.f;

        Quaternion qRot = Quaternion::fromAngleAxis(deg,randomAxis);

        for (int i = 0; i < 8; i++) {
            q = q * qRot;
        }

        assert(areEquivalent(q, Quaternion::identity()));
        assert(areEqual(q, Quaternion::identity()));
    }


}

void unitTestTransformation(){
    Transform t;
    t.rotate = Quaternion::fromAngleAxis( 43.4f, Vector3(1,-3,-2) );
    t.translate = Vector3(1,3,4);
    t.scale = 5;

    Point3 p(4,10,-13);

    Point3 q = t.transformPoint( p );
    Point3 r = t.inverse().transformPoint( q );

    assert( areEqual(p,r) );

    Transform ti = t;
    ti.invert();
    r = ti.transformPoint( q );
    assert( areEqual(p,r) );

    Transform tA;
    tA.rotate = Quaternion::fromAngleAxis( -13.4f, Vector3(13,4,0) );
    tA.translate = Vector3(0,-1,01);
    tA.scale = 0.23;

    Transform tB = t;

    Transform tAB = tA*tB;
    assert( areEqual(
              tAB.transformPoint(p),
              tA.transformPoint( tB.transformPoint(p) )
            )
          );


}

inline bool IsKeyPressed(char key) {
    return GetAsyncKeyState(key) & 0x8000;
}

bool SwitchPOV(Transform& Requested, std::vector<GameObj>& objs, GameObj*& Tracking) {
    if (IsKeyPressed('0') && objs.size() > 0) {
        Requested = Transform();
        if(!IsKeyPressed(VK_SHIFT)) Tracking = nullptr;
        return true;
    }
    else
    {
        for (char c = '1'; atoi(&c) <= objs.size() && c <= '9'; c++)
        {
            int KeyPressedAsNumber = atoi(&c);
            if (IsKeyPressed(c) && KeyPressedAsNumber - 1 < objs.size() - 1) //-1 to ignore floor which is set as last one
            {
				Requested = objs[KeyPressedAsNumber-1].transform;
				if (!IsKeyPressed(VK_SHIFT)) Tracking = &(objs[KeyPressedAsNumber-1]);
                return true;
            }

        }

		if (IsKeyPressed(VK_SPACE)) {
			Tracking = nullptr;
			Requested.translate = { 0,5,0 };
			Requested.rotate = Quaternion::fromAngleAxis(30, { 1, 0, 0 });
			Requested.scale = 1;
            return true;
        }

        return false;
    }
}


int main(){

    unitTestLinearOps();
    unitTestProdutcs();
    unitTestRaycasts();
    unitTestRaycastPlane();

    unitTestQuaternions();
    unitTestTransformation();


    Scene s;
    s.populate(5);

	GameObj floor = { new Plane({0,-1,0}, Versor3({0,1,0}).normalized())};
	s.obj.push_back(floor);

    GameObj* Tracking = nullptr;
    Transform currentPosition;
    Transform RequestedInterp;
    rayCasting(s.toView(currentPosition));

    float RotationDelta = 3.f;
    float MovementDelta = 0.5f;

    bool wasShiftPressed = false;

    while(true){
        if (SwitchPOV(RequestedInterp, s.obj, Tracking)) {
            Transform Starting = currentPosition;

			for (Scalar i = 0.f; i <= 1.f; i += 0.05f) {
                currentPosition = Lerp(Starting, RequestedInterp, i);
                mgd::rayCasting(s.toView(currentPosition));
                Sleep(16);
			}
            currentPosition = RequestedInterp;
        };
        Transform DeltaTransform = Transform();
		if (IsKeyPressed('W')) {
            DeltaTransform.translate += MovementDelta * Vector3(0, 0, 1);
		}
		if (IsKeyPressed('A')) {
            DeltaTransform.rotate = Quaternion::fromAngleAxis(-RotationDelta, Vector3(0, 1, 0));
		}
		if (IsKeyPressed('S')) {
            DeltaTransform.translate += -MovementDelta * Vector3(0, 0, 1);
		}
		if (IsKeyPressed('D')) {
            DeltaTransform.rotate = Quaternion::fromAngleAxis(RotationDelta, Vector3(0, 1, 0));
		}

        if(!IsKeyPressed(VK_SHIFT)) currentPosition = currentPosition * DeltaTransform;
        if(Tracking) Tracking->transform = Tracking->transform * DeltaTransform;

        //Return to tracked if you stop pressing shift
		if (Tracking && wasShiftPressed && !IsKeyPressed(VK_SHIFT)) {
			Transform Starting = currentPosition;
            RequestedInterp = Tracking->transform;
			for (Scalar i = 0.f; i <= 1.f; i += 0.05f) {
				currentPosition = Lerp(Starting, RequestedInterp, i);
				mgd::rayCasting(s.toView(currentPosition));
				Sleep(16);
			}
			currentPosition = Tracking->transform;
		}
        wasShiftPressed = IsKeyPressed(VK_SHIFT);

        mgd::rayCasting(s.toView(currentPosition));
        Sleep(16);
    }
}
