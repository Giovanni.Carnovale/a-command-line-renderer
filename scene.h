#pragma once
#include <vector>
#include <string>
#include <iostream>
#include <windows.h>

#include "transform.h"
#include "shapes3d.h"
#include "camera.h"

namespace mgd{

class Scene{
public:
    std::vector<GameObj> obj; // a set with GameObj (each with its own transform)
    mutable std::vector<Mesh*> res;

    Scene() = default;
    Scene(const Scene& ) = delete;
    Scene& operator=(const Scene&) = delete;

    ~Scene()
    {
        for (GameObj& o : obj)
        {
            for (Mesh*& m : o.meshArray)
            {
                delete m;
                m = nullptr;
            }
        }
    }

    void populate(int n){
        for (int i=0;i<n;i++){
            GameObj someoneNew = { new Sphere(Vector3(0, 0, 0), 1), new Sphere(Vector3(0.5f, 0.75f, 0.5f), 0.5f), new Sphere(Vector3(-0.5f, 0.75f, 0.5f), 0.5f) };
            someoneNew.transform.translate = Vector3::random(6) + Vector3(0,0,15);
            someoneNew.transform.translate.y = 0;
            obj.push_back(  someoneNew );
        }
    }

    std::vector<Mesh*> toWorld() const{
        for(Mesh* &m : res)
            delete m;
        res.clear();

        for (const GameObj &g : obj) {
            for(Mesh* pMesh : g.meshArray)
                res.push_back( pMesh->apply( g.transform ) );
        }
        return res;
    }

	std::vector<Mesh*> toView(Transform camera) const {
		for (Mesh*& m : res)
			delete m;
		res.clear();

		for (const GameObj& g : obj) {
            if(g.transform == camera) continue;

			for(Mesh* pMesh : g.meshArray)
                res.push_back( pMesh->apply(camera.inverse() * g.transform ) );
		}
		return res;
	}

};

// ascii art: convert an intensity value (0 to 1) into a sequence of two chars
const char* intensityToCstr(Scalar intensity){
    switch( int (round(intensity*8 ))) {
    case 0: return "  "; // darkest
    case 1: return " '";
    case 2: return " +";
    case 3: return " *";
    case 4: return " #";
    case 5: return "'#";
    case 6: return "+#";
    case 7: return "*#";
    case 8: return "##"; // lightest
    default:
    case 10: return "##";
    }
}


const char* lighting( Versor3 normal ){
    Versor3 lightDir = Versor3(1,2,-2).normalized();
    // lambertian
    Scalar diffuse = dot( normal, lightDir );
    if (diffuse < 0) diffuse = 0;

    return intensityToCstr( diffuse );
}

void rayCasting(const std::vector<Mesh*>& meshesVector){

    Camera c(5, 64,52);
    HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD tl = {0,0};
    SetConsoleCursorPosition(console, tl);

    std::string screenBuffer ; // a string to get ready and print all at once

    for (int y = 0; y<c.pixelDimY; y++) {
        for (int x= 0; x<c.pixelDimX; x++) {
            Point3 hitPos;
            Point3 hitNorm;
            Scalar distMax = 100.0;

            for  (Mesh* s : meshesVector) {
               s->rayCast( c.primaryRay(x,y)  , hitPos , hitNorm , distMax );
            }
            screenBuffer += lighting( hitNorm );
        }
        screenBuffer +=  "\n";
    }
    std::cout << screenBuffer;
}


} // end of namespace mgd

